import React, { PureComponent, Fragment } from 'react';
import moment from 'moment-timezone';
import Clock from 'components/Clock';
import TimePicker from 'components/TimePicker';
import Button from 'components/Button';
import Footer from 'components/Footer';


class Home extends PureComponent {

  constructor(props) {
    super(props);

    this.state = {
      moment: moment()
    }
  }

  componentDidMount() {
    this.runTimer();
  }

  runTimer = () => {

    this.stopTimer();

    this.interval = setInterval(() => {
      this.setState({
        moment: moment(this.state.moment).add(1, 'seconds')
      })
    }, 1000)
  }

  stopTimer = () => {
    return clearInterval(this.interval);
  }

  _eventTimePickerChange = time => {

    this.setState({
      moment: time
    })
  }

  refreshTime = () => {
    this.setState({
      moment: moment()
    })
  }

  render() {
    const { state } = this;

    return (
      <Fragment>
        <div className="page-body">
          <div className="content">
            <Clock
              moment={state.moment}
              timezone='Europe/London'
              city='London'
            />
            <Clock
              moment={state.moment}
            />
            <Clock
              moment={state.moment}
              timezone='America/New_York'
              city='New York'
            />
          </div>
        </div>

        <Footer>
          <TimePicker
            moment={state.moment}
            onChange={this._eventTimePickerChange}
          />

          <Button
            onClick={this.refreshTime}
          >
            Cброс
        </Button>
        </Footer>
      </Fragment>
    )
  }
}

Home.propTypes = {

};

Home.defaultProps = {

};

export default Home;
