import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment-timezone';

import './style.scss'


class Clock extends PureComponent {

  getTime = () => {
    const { props } = this;
    const timeIsNow = moment(props.moment).tz(props.timezone);
    const hours = parseInt(timeIsNow.format('hh'));
    const minutes = parseInt(timeIsNow.format('mm'));
    const seconds = parseInt(timeIsNow.format('ss'));

    return {
      hours,
      minutes,
      seconds
    }
  }

  _renderSeconds = seconds => {
    const drawSeconds = ((seconds / 60) * 360) + 90;
    const style = {
      transform: `rotate(${drawSeconds}deg)`,
      transition: drawSeconds === 444 || drawSeconds === 90 ? "all 0s ease 0s" : "all 0.05s cubic-bezier(0, 0, 0.52, 2.51) 0s"
    }

    return (
      <div className="hand seconds" style={style} />
    )
  }

  _renderMinutes = minutes => {
    const drawMinutes = ((minutes / 60) * 360) + 90;
    const style = {
      transform: `rotate(${drawMinutes}deg)`,
    }

    return (
      <div className="hand minutes" style={style} />
    )
  }

  _renderHours = hours => {
    const drawHours = ((hours / 12) * 360) + 90;
    const style = {
      transform: `rotate(${drawHours}deg)`,
    }

    return (
      <div className="hand hours" style={style} />
    )
  }



  render() {
    const { props } = this;

    return (
      <div className="widget clock" id="seattle">
        <div className="shadow"></div>
        {this._renderSeconds(this.getTime().seconds)}
        {this._renderMinutes(this.getTime().minutes)}
        {this._renderHours(this.getTime().hours)}
        <div className="hand-cap"></div>
        <label>{props.city}</label>
      </div>
    )
  }
}

Clock.propTypes = {
  timezone: PropTypes.string,
  city: PropTypes.string,
};

Clock.defaultProps = {
  timezone: 'Europe/Kiev',
  city: 'Киев'
};

export default Clock;
