import React, { Component } from 'react';
import { TimePicker as Timepicker } from 'antd';
import PropTypes from 'prop-types';

class TimePicker extends Component {

  render() {
    const { props } = this;

    return (
      <Timepicker
        defaultValue={props.moment}
        value={props.moment}
        onChange={props.onChange}
      />
    )
  }
}

TimePicker.propTypes = {
  defaultValue: PropTypes.object, // moment
  value: PropTypes.object, // moment
  onChange: PropTypes.func,
};

TimePicker.defaultProps = {

};

export default TimePicker
