import React, { PureComponent } from 'react';
import './style.scss'


class Footer extends PureComponent {

  render() {
    const { props } = this;

    return (
      <footer className='footer'>
        <div className='container'>
          <div className="footer__content">
            {props.children}
          </div>
        </div>
      </footer>
    )
  }
}

export default Footer
