import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import './style.scss'


class Header extends PureComponent {

  render() {
    const { props } = this;

    return (
      <header className='header'>
        <div className='container'>
          <h1>{props.title}</h1>
          {props.children}
        </div>
      </header>
    )
  }
}

Header.propTypes = {
  title: PropTypes.string
};

Header.defaultProps = {
  title: 'Title'
};

export default Header
