import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Button as ButtonAnt } from 'antd';

import 'antd/dist/antd.css';



class Button extends PureComponent {

  onClick = () => {
    const { props } = this;
    console.log('props.onClick ', props.onClick)
    if (typeof props.onClick === 'function') {
      props.onClick();
    }
  }

  render() {
    const { props } = this;
    const { children, ...params } = props;

    return (
      <ButtonAnt
        className='button'
        {...params}
      >
        {children}
      </ButtonAnt>
    )
  }
}

Button.propTypes = {
  size: PropTypes.string,
  type: PropTypes.string,
  onClick: PropTypes.func,
};

Button.defaultProps = {
  size: "large",
  type: "primary",
};

export default Button;

