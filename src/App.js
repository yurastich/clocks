import React from 'react';
import Header from 'components/Header';
import Home from 'pages/Home';

import 'antd/dist/antd.css';
import './style.scss'



function App() {
  return (
    <div className='page-wrapper'>
      <Header
        title='Test app'
      />

      <Home />

    </div>
  );
}

export default App;
